//
// Created by matteof on 9/30/20.
//

#include <boost/filesystem.hpp>

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include "Config.hpp"
#include "NTOFException.h"
#include "local-config.h"
#include "test_helpers.hpp"

namespace bfs = boost::filesystem;
using namespace ntof::alarms;
using namespace ntof;

class TestConfig : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestConfig);
    CPPUNIT_TEST(test_Config_Exceptions);
    CPPUNIT_TEST(test_FullConfig);
    CPPUNIT_TEST_SUITE_END();

protected:
    bfs::path dataDir_;

public:
    void setUp() override { dataDir_ = bfs::path(SRCDIR) / "tests" / "data"; }

    void tearDown() override
    {
        Config::load((dataDir_ / "pressureAlarm.xml").string(),
                     (dataDir_ / "configMisc.xml").string());
    }

    void test_Config_Exceptions()
    {
        CPPUNIT_ASSERT_THROW(
            Config::load((dataDir_ / "no_existing_file.xml").string(),
                         (dataDir_ / "configMisc.xml").string()),
            NTOFException);

        CPPUNIT_ASSERT_THROW(
            Config::load((dataDir_ / "pressureAlarm.xml").string(),
                         (dataDir_ / "no_existing_file.xml").string()),
            NTOFException);
    }

    void test_FullConfig()
    {
        Config::load((dataDir_ / "pressureAlarm.xml").string(),
                     (dataDir_ / "configMisc.xml").string());
        Config &conf = Config::instance();
        EQ(std::string("ntof-door-pressure"), conf.getServerName());

        const std::vector<PressureDetails> &details = conf.getPressureDetails();
        EQ((size_t) 4, details.size());

        // Test full definition
        const PressureDetails &fullDetail = details.front();
        EQ(std::string("CV.UVVA_PRESDIF_001"), fullDetail.source);
        EQ(std::string("CHANGING_ROOM_UVVA_0001"), fullDetail.destination);
        EQ(std::string("dataValue"), fullDetail.field);
        EQ((uint32_t) 500, fullDetail.delay);
        DBL_EQ(double(100), fullDetail.minThreshold, 0.01);
        DBL_EQ(double(200), fullDetail.maxThreshold, 0.01);

        // Test Default values
        const PressureDetails &defDetail = details.back();
        EQ(std::string("CV.UVVA_PRESDIF_005"), defDetail.source);
        EQ(std::string("LOBBY_UVVA_0005"), defDetail.destination);
        EQ(std::string("value"), defDetail.field);
        EQ((uint32_t) 300, defDetail.delay);
        DBL_EQ(double(0), defDetail.minThreshold, 0.01);
        DBL_EQ(double(0), defDetail.maxThreshold, 0.01);
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestConfig);
