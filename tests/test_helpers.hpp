/*
** Copyright (C) 2018 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2018-10-09T13:27:50+02:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#ifndef TEST_HELPERS_HPP__
#define TEST_HELPERS_HPP__

#include <condition_variable>
#include <functional>
#include <mutex>
#include <string>

#include <Signals.hpp>
#include <cppunit/TestAssert.h>
#include <cppunit/extensions/HelperMacros.h>
#include <unistd.h>

// reduce number of includes in test files
#include "test_helpers_dim.hpp"

#define EQ CPPUNIT_ASSERT_EQUAL
#define ASSERT CPPUNIT_ASSERT
#define ASSERT_THROW CPPUNIT_ASSERT_THROW
#define DBL_EQ CPPUNIT_ASSERT_DOUBLES_EQUAL

void printException(const CPPUNIT_NS::Exception &ex);
void assertContains(const std::string &haystack,
                    const std::string &needle,
                    const CPPUNIT_NS::SourceLine &line);

#define CPPUNIT_ASSERT_CONTAINS(list, key) \
    assertContains(list, key, CPPUNIT_SOURCELINE())

#define EQ CPPUNIT_ASSERT_EQUAL
#define CONTAINS CPPUNIT_ASSERT_CONTAINS

CPPUNIT_NS_BEGIN

#define CPPUNIT_TRAITS_OVERRIDE(TYPE) \
    template<>                        \
    std::string assertion_traits<TYPE>::toString(const TYPE &t)

CPPUNIT_NS_END

pid_t aspawn(const char *const argv[]);

template<typename... Arg>
pid_t spawn(const char *file, Arg... args)
{
    const char *const list[] = {file, args..., 0};
    return aspawn(list);
}

void terminate(pid_t pid);

#endif // TEST_HELPERS_HPP__
