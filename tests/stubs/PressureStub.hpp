//
// Created by matteof on 9/30/20.
//

#include <memory>

#include <DIMDataSet.h>

#ifndef PRESSUREALARM_PRESSURESTUB_HPP
#define PRESSUREALARM_PRESSURESTUB_HPP

#define DEF_MIN_T 100.0
#define DEF_MAX_T 300.0

struct PressureStubDetail
{
    std::string field;
    double value;
    double minThreshold;
    double maxThreshold;

    PressureStubDetail() :
        field("value"),
        value(0.0),
        minThreshold(DEF_MIN_T),
        maxThreshold(DEF_MAX_T)
    {}
};

class PressureStub
{
public:
    explicit PressureStub(const std::string &serviceName,
                          PressureStubDetail &details);
    ~PressureStub() = default;

    void setValue(double value);
    const std::string &getServiceName() const;

protected:
    std::unique_ptr<ntof::dim::DIMDataSet> m_valueSvc;
    std::unique_ptr<ntof::dim::DIMDataSet> m_lowerSvc;
    std::unique_ptr<ntof::dim::DIMDataSet> m_upperSvc;

    std::string m_serviceName;
    PressureStubDetail m_details;
};

#endif // PRESSUREALARM_PRESSURESTUB_HPP
