//
// Created by matteof on 9/30/20.
//

#include "PressureStub.hpp"

#include <easylogging++.h>

using namespace ntof::dim;

PressureStub::PressureStub(const std::string &serviceName,
                           PressureStubDetail &details) :
    m_serviceName(serviceName), m_details(details)
{
    m_valueSvc.reset(new ntof::dim::DIMDataSet(m_serviceName));
    m_lowerSvc.reset(new DIMDataSet(m_serviceName + "_LL"));
    m_upperSvc.reset(new DIMDataSet(m_serviceName + "_HH"));

    // value alarm service
    m_valueSvc->addData(0, "FakeData", "", (uint32_t) 0, AddMode::CREATE, false);
    m_valueSvc->addData(1, m_details.field, "", (double) m_details.value,
                        AddMode::CREATE, true);

    // lower alarm service
    m_lowerSvc->addData(0, "FakeData", "", (uint32_t) 0, AddMode::CREATE, false);
    m_lowerSvc->addData(1, m_details.field, "", (double) m_details.minThreshold,
                        AddMode::CREATE, true);

    // upper alarm service
    m_upperSvc->addData(0, "FakeData", "", (uint32_t) 0, AddMode::CREATE, false);
    m_upperSvc->addData(1, m_details.field, "", (double) m_details.maxThreshold,
                        AddMode::CREATE, true);

    LOG(INFO) << "Stub for service: " << m_serviceName << " is ready;";
}

void PressureStub::setValue(double value)
{
    m_valueSvc->lockDataAt(1)->setValue(value);
    LOG(INFO) << "Updated value to [" << value << "] "
              << "for service: " << m_serviceName;
}

const std::string &PressureStub::getServiceName() const
{
    return m_serviceName;
}
