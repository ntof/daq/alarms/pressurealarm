
if(TESTS)

link_directories(
  ${CMAKE_BINARY_DIR}/src
  ${DIM_LIBRARIES}
  ${CPPUNIT_LIBRARY_DIRS})
include_directories(${CMAKE_BINARY_DIR} ${CMAKE_SOURCE_DIR}/src)
include_directories(SYSTEM
  ${CPPUNIT_INCLUDE_DIRS}
  ${DIM_INCLUDE_DIRS}
  ${NTOFUTILS_INCLUDE_DIRS}
  ${Boost_INCLUDE_DIRS})

set(test_SRCS

  stubs/PressureStub.hpp stubs/PressureStub.cpp

  test_main.cpp test_init.cpp
  test_helpers.hpp test_helpers.cpp
  test_helpers_dim.hpp test_helpers_dim.cpp
  test_Config.cpp
  test_PressureAlarm.cpp
  )

add_executable(test_all ${test_SRCS})
target_link_libraries(test_all PressureAlarmLib ntofutils
  ${CPPUNIT_LIBRARIES}
  ${DIM_LIBRARIES} ${Boost_LIBRARIES} ${PUGI_LIBRARIES})

add_test(test_all ${CMAKE_CURRENT_BINARY_DIR}/test_all)

endif(TESTS)
