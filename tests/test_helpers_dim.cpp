/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-01-06T16:27:02+01:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include <chrono>
#include <thread>

#include <boost/filesystem.hpp>

#include "local-config.h"   // BUILDDIR
#include "test_helpers.hpp" // spawn, terminate

#include <dis.hxx>

namespace bfs = boost::filesystem;

DimInfoWaiter::DimInfoWaiter(const std::string &name) :
    DimInfo(name.c_str(), static_cast<void *>(0), 0), m_count(0)
{}

void DimInfoWaiter::reset()
{
    std::unique_lock<std::mutex> lock(m_lock);
    m_count = 0;
}

bool DimInfoWaiter::waitUpdate(unsigned int count, unsigned int ms)
{
    std::unique_lock<std::mutex> lock(m_lock);

    while (m_count < count)
    {
        if (m_cond.wait_for(lock, std::chrono::milliseconds(ms)) ==
            std::cv_status::timeout)
            break;
    }
    return m_count >= count;
}

void DimInfoWaiter::infoHandler()
{
    DimInfo::infoHandler();
    {
        std::unique_lock<std::mutex> lock(m_lock);
        if (this->getData() != 0) /* handles nolink */
            ++m_count;
    }
    m_cond.notify_all();
}

DimTestHelper::DimTestHelper()
{
    const std::string dnsExe =
        (bfs::path(BUILDDIR) / "instroot" / "bin" / "dns").string();
    if (std::getenv("DNS_DEBUG") != nullptr)
    {
        m_pid = ::spawn(dnsExe.c_str(), "-d");
    }
    else
    {
        m_pid = ::spawn(dnsExe.c_str());
    }
    CPPUNIT_ASSERT_MESSAGE("Failed to start DIM DNS", m_pid >= 0);
    std::this_thread::sleep_for(std::chrono::seconds(3));

    DimServer::start("testServer");
}

DimTestHelper::~DimTestHelper()
{
    if (m_pid > 0)
    {
        terminate(m_pid);
    }
    dic_stop();
    DimServer::stop();
}
