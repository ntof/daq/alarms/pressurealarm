/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-09-30T10:43:48+01:00
**     Author: Matteo Ferrari <matteof> <matteo.ferrari.1@cern.ch>
**
*/

#include <memory>

#include <boost/filesystem.hpp>

#include <DIMData.h>
#include <DIMException.h>
#include <DIMStateClient.h>
#include <NTOFLogging.hpp>
#include <Signals.hpp>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <easylogging++.h>
#include <math.h>

#include "Config.hpp"
#include "PressureAlarm.hpp"
#include "local-config.h"
#include "stubs/PressureStub.hpp"
#include "test_helpers.hpp"

#include <dis.hxx>

namespace bfs = boost::filesystem;
using namespace ntof::alarms;
using namespace ntof::utils;
using ntof::dim::DIMData;
using ntof::dim::DIMException;

bool AreSame(double a, double b)
{
    return fabs(a - b) < std::numeric_limits<double>::epsilon();
}

class TestPressureAlarm : public CppUnit::TestFixture
{
protected:
    CPPUNIT_TEST_SUITE(TestPressureAlarm);
    CPPUNIT_TEST(simple);
    CPPUNIT_TEST_SUITE_END();

    typedef std::unique_ptr<PressureStub> PressureStubPtr;
    std::vector<PressureStubPtr> m_stubs;
    bfs::path dataDir_;
    std::unique_ptr<DimTestHelper> m_dim;

public:
    void setUp() override
    {
        dataDir_ = bfs::path(SRCDIR) / "tests" / "data";
        Config::load((dataDir_ / "pressureAlarm_unittest.xml").string(),
                     (dataDir_ / "configMisc.xml").string());

        m_dim.reset(new DimTestHelper());

        for (const PressureDetails &detail :
             Config::instance().getPressureDetails())
        {
            PressureStubDetail stubDetail;
            stubDetail.field = detail.field;
            if (!AreSame(detail.minThreshold, 0.0))
            {
                stubDetail.minThreshold = detail.minThreshold;
            }
            if (!AreSame(detail.maxThreshold, 0.0))
            {
                stubDetail.maxThreshold = detail.maxThreshold;
            }
            m_stubs.emplace_back(new PressureStub(detail.source, stubDetail));
        }
    }

    void tearDown() override
    {
        m_stubs.clear();
        m_dim.reset();
    }

    void simple()
    {
        EQ((size_t) 2, Config::instance().getPressureDetails().size());

        // First alarm with min 100 and max 200
        PressureStubPtr &stub01 = m_stubs.front();

        const PressureDetails &detail01 =
            Config::instance().getPressureDetails().front();
        std::unique_ptr<PressureAlarm> alarm01(new PressureAlarm(detail01));
        ntof::dim::DIMStateClient stateClient(detail01.destination);
        SignalWaiter stateWaiter;
        stateWaiter.listen(stateClient.dataSignal);

        stateWaiter.wait([&]() {
            return stateClient.getActualState() ==
                PressureAlarm::StateAlarm::CONNECTING;
        });

        // Set value of pressure to 50 (out of range!)
        stub01->setValue(50);

        stateWaiter.wait([&]() {
            return stateClient.getActualState() == PressureAlarm::StateAlarm::OK;
        });

        // Check alarm
        alarm01->checkAlarm();

        stateWaiter.wait([&]() {
            return stateClient.getActiveErrors().size() == 1 &&
                stateClient.getActiveErrors().front().getCode() ==
                PressureAlarm::Errors::PRESSURE_OUT_OF_RANGE;
        });

        // Set value of pressure to 150 (in range). It should clear error
        stub01->setValue(150);
        stateWaiter.wait([&]() {
            return stateClient.getActiveErrors().size() == 0 &&
                stateClient.getActualState() == PressureAlarm::StateAlarm::OK;
        });
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestPressureAlarm);
