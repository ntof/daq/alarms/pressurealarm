/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-09-29T11:16:40+02:00
**     Author: Matteo Ferrari <matteof> <matteo.ferrari.1@cern.ch>
**
**
*/

#ifndef PRESSUREALARM_ALARMTYPES_HPP
#define PRESSUREALARM_ALARMTYPES_HPP

#include <cstdint>
#include <string>

namespace ntof {
namespace alarms {

struct PressureDetails
{
    std::string source;
    std::string destination;
    uint32_t delay;
    std::string field;
    double minThreshold;
    double maxThreshold;

    PressureDetails(std::string &iSource,
                    std::string &iDestination,
                    uint32_t iDelay,
                    std::string &iField,
                    double iMin,
                    double iMax) :
        source(std::move(iSource)),
        destination(std::move(iDestination)),
        delay(iDelay),
        field(std::move(iField)),
        minThreshold(iMin),
        maxThreshold(iMax)
    {}
};

} /* namespace alarms */
} /* namespace ntof */

#endif // PRESSUREALARM_ALARMTYPES_HPP
