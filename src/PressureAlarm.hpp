/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-09-25T11:17:40+02:00
**     Author: mdonze
**
*/

#ifndef PRESSUREALARM_HPP_
#define PRESSUREALARM_HPP_

#include <memory>
#include <mutex>
#include <string>

#include <DIMProxyClient.h>
#include <DIMState.h>

#include "AlarmTypes.hpp"

namespace ntof {
namespace alarms {

/**
 * This class is used to create and publish an alarm
 * when someone forgot to close the n_TOF EAR2 door...
 */
class PressureAlarm : private ntof::dim::DIMProxyClientHandler
{
public:
    enum StateAlarm
    {
        NO_LINK_STATE = -3,
        ERROR = -2,
        NOT_READY = -1,
        CONNECTING = 0,
        OK
    };

    enum Errors
    {
        PRESSURE_OUT_OF_RANGE = 1,
        CLIENT_CONNECTION_ERROR = 2
    };

    explicit PressureAlarm(const PressureDetails &details);
    ~PressureAlarm() override = default;

    /**
     * Called every second from the main thread
     */
    void checkAlarm();

    /**
     * Infinite loop that manage all the alarms
     */
    [[noreturn]] static void runner();

protected:
    /**
     * Called when the DIM service is refreshed by the server
     * @param dataSet New list of data
     * @param client Client responsible of this callback (can be shared)
     */
    void dataChanged(std::vector<ntof::dim::DIMData> &dataSet,
                     const ntof::dim::DIMProxyClient &client) override;

    /**
     * Called when an error occurred on client (NO-LINK, NOT READY...)
     * @param errMsg Error message
     * @param client Client responsible for this callback (can be shared)
     */
    void errorReceived(const std::string &errMsg,
                       const ntof::dim::DIMProxyClient &client) override;

    std::shared_ptr<ntof::dim::DIMProxyClient> m_valueClient;
    std::shared_ptr<ntof::dim::DIMProxyClient> m_lowerAlarmClient;
    std::shared_ptr<ntof::dim::DIMProxyClient> m_upperAlarmClient;
    std::shared_ptr<ntof::dim::DIMState> m_state;

    PressureDetails m_details;
    uint32_t m_elapsed;
    std::mutex m_mutex;
};

} /* namespace alarms */
} /* namespace ntof */

#endif /* PRESSUREALARM_HPP_ */
