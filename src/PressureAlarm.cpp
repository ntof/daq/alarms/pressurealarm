/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-09-25T11:17:40+02:00
**     Author: mdonze
**
*/

#include "PressureAlarm.hpp"

#include <sstream>

#include "Config.hpp"

namespace ntof {
namespace alarms {

PressureAlarm::PressureAlarm(const PressureDetails &details) :
    m_details(details), m_elapsed(0)
{
    m_valueClient.reset(new ntof::dim::DIMProxyClient(m_details.source));
    m_valueClient->setHandler(this);

    m_state.reset(new ntof::dim::DIMState(m_details.destination));
    m_state->addStateValue(StateAlarm::CONNECTING, "CONNECTING", false);
    m_state->addStateValue(StateAlarm::OK, "OK", true);
    m_state->setValue(StateAlarm::CONNECTING);

    if (m_details.minThreshold == 0.0)
    {
        std::string svcName = m_details.source + "_LL";
        m_lowerAlarmClient.reset(new ntof::dim::DIMProxyClient(svcName));
        m_lowerAlarmClient->setHandler(this);
    }

    if (m_details.maxThreshold == 0.0)
    {
        std::string svcName = m_details.source + "_HH";
        m_upperAlarmClient.reset(new ntof::dim::DIMProxyClient(svcName));
        m_upperAlarmClient->setHandler(this);
    }
}

void PressureAlarm::dataChanged(std::vector<ntof::dim::DIMData> &dataSet,
                                const ntof::dim::DIMProxyClient &client)
{
    if ((m_state->getValue() < StateAlarm::OK) &&
        (m_state->getValue() != StateAlarm::ERROR))
    {
        m_state->setValue(StateAlarm::OK);
    }

    double value = 0.0;

    for (const ntof::dim::DIMData &data : dataSet)
    {
        if (data.getName() == m_details.field)
        {
            value = data.getDoubleValue();
            break;
        }
    }

    if (client == *m_valueClient)
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        std::cout << "Value : " << value << " Min : " << m_details.minThreshold
                  << " Max : " << m_details.maxThreshold
                  << " Elapsed : " << m_elapsed << std::endl;
        if ((value < m_details.minThreshold) || (value > m_details.maxThreshold))
        {
            // Raise alarm?
        }
        else
        {
            // Clear error
            if (m_elapsed != 0)
            {
                std::cout << "Error cleared" << std::endl;
                m_state->clearError();
            }
            m_elapsed = 0;
        }
    }
    else if ((m_lowerAlarmClient != nullptr) && (client == *m_lowerAlarmClient))
    {
        m_details.minThreshold = value;
    }
    else if ((m_upperAlarmClient != nullptr) && (client == *m_upperAlarmClient))
    {
        m_details.maxThreshold = value;
    }
}

void PressureAlarm::errorReceived(const std::string &errMsg,
                                  const ntof::dim::DIMProxyClient &client)
{
    std::stringstream oss;
    if (client == *m_valueClient)
    {
        oss << "Value client error : " << errMsg;
    }
    else if ((m_lowerAlarmClient != nullptr) && (client == *m_lowerAlarmClient))
    {
        oss << "Lower alarm client error : " << errMsg;
    }
    else if ((m_upperAlarmClient != nullptr) && (client == *m_upperAlarmClient))
    {
        oss << "Upper alarm client error : " << errMsg;
    }
    m_state->setError(Errors::CLIENT_CONNECTION_ERROR, oss.str());
}

/**
 * Called every second from the main thread
 */
void PressureAlarm::checkAlarm()
{
    std::lock_guard<std::mutex> lock(m_mutex);
    ++m_elapsed;
    if (m_elapsed > m_details.delay)
    {
        m_state->setError(Errors::PRESSURE_OUT_OF_RANGE,
                          "Pressure out of range!");
    }
}

[[noreturn]] void PressureAlarm::runner()
{
    std::vector<std::unique_ptr<PressureAlarm>> alarms;
    for (const PressureDetails &details :
         Config::instance().getPressureDetails())
    {
        alarms.emplace_back(new PressureAlarm(details));
    }

    // Perform loop
    while (true)
    {
        struct timespec tv
        {};
        tv.tv_sec = 1;
        tv.tv_nsec = 0;
        nanosleep(&tv, nullptr);
        for (const std::unique_ptr<PressureAlarm> &alarm : alarms)
        {
            alarm->checkAlarm();
        }
    }
}

} /* namespace alarms */
} /* namespace ntof */
