/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-09-25T11:17:40+02:00
**     Author: Matteo Ferrari <matteof> <matteo.ferrari.1@cern.ch>
**
** Original code by mdonze
**
*/

#include <string>
#include <vector>

#include <NTOFException.h>
#include <easylogging++.h>

#include "Config.hpp"
#include "PressureAlarm.hpp"

#include <dis.hxx>

using namespace ntof::alarms;

int main(int argc, char **argv)
{
    START_EASYLOGGINGPP(argc, argv);
    try
    {
        const std::string configFile = (argc > 1) ? std::string(argv[1]) :
                                                    Config::configFile;
        const std::string configMiscFile = (argc > 2) ? std::string(argv[2]) :
                                                        Config::configMiscFile;

        Config::load(configFile, configMiscFile);

        // Initialized DIM parameters
        DimServer::setDnsNode(Config::instance().getDimDns().c_str());
        DimClient::setDnsNode(Config::instance().getDimDns().c_str());

        DimServer::start(Config::instance().getServerName().c_str());

        PressureAlarm::runner();
    }
    catch (const ntof::NTOFException &ex)
    {
        LOG(ERROR)
            << "[FATAL ERROR] Caught NTOFException : " << ex.getMessage();
    }
    catch (const std::exception &ex)
    {
        LOG(ERROR) << "[FATAL ERROR] Caught std::exception : " << ex.what();
    }
    catch (...)
    {
        LOG(ERROR) << "[FATAL ERROR] Caught unknown exception";
    }
}
