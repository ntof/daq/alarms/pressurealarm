/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-09-29T11:16:40+02:00
**     Author: Matteo Ferrari <matteof> <matteo.ferrari.1@cern.ch>
**
** Original code by agiraud
**
*/

#include "Config.hpp"

#include <string>

#include <NTOFException.h>
#include <pugixml.hpp>

#include <Singleton.hxx>

template class ntof::utils::Singleton<ntof::alarms::Config>;

namespace ntof {
namespace alarms {

const std::string Config::configFile = "/etc/ntof/pressureAlarm.xml";
const std::string Config::configMiscFile = "/etc/ntof/misc.xml";

Config::Config(const std::string &file, const std::string &miscFile) :
    ConfigMisc(miscFile)
{
    pugi::xml_document doc;

    // Read the config file
    pugi::xml_parse_result result = doc.load_file(file.c_str());
    if (!result)
    {
        throw NTOFException("Unable to read the configuration file : " + file,
                            __FILE__, __LINE__);
    }

    // Parse the config file
    pugi::xml_node root = doc.root().child("configuration");

    m_serverName = root.child("serverName")
                       .attribute("value")
                       .as_string("ntof-door-pressure");

    pugi::xml_node alarmsNode = root.child("alarms");
    if (alarmsNode)
    {
        for (pugi::xml_node alarm = alarmsNode.first_child(); alarm;
             alarm = alarm.next_sibling())
        {
            if (!alarm.attribute("source") || !alarm.attribute("destination"))
            {
                throw NTOFException(
                    "Bad DIM service description. "
                    "Expected at least source and destination attributes : ",
                    __FILE__, __LINE__);
            }
            std::string source = alarm.attribute("source").as_string();
            std::string destination = alarm.attribute("destination").as_string();
            uint32_t delay = alarm.attribute("delay").as_int(300);
            std::string field = alarm.attribute("field").as_string("value");
            double minThreshold = alarm.attribute("min").as_double(0.0);
            double maxThreshold = alarm.attribute("max").as_double(0.0);
            m_pressureDetails.emplace_back(source, destination, delay, field,
                                           minThreshold, maxThreshold);
        }
    }
}

Config &Config::load(const std::string &file, const std::string &miscFile)
{
    ntof::utils::SingletonMutex lock;

    if (m_instance)
    {
        delete m_instance;
        m_instance = nullptr;
    }

    m_instance = new Config(file, miscFile);
    return *m_instance;
}

const std::string &Config::getServerName() const
{
    return m_serverName;
}

const std::vector<PressureDetails> &Config::getPressureDetails() const
{
    return m_pressureDetails;
}

} /* namespace alarms */
} /* namespace ntof */
