
# Do not use GNUInstallDirs there (would use lib64 instead of lib)
install(FILES pressurealarm.service
  DESTINATION "lib/systemd/system"
  COMPONENT Runtime)
