
include(Tools)

find_path(PUGI_INCLUDE_DIRS NAMES pugixml.hpp)
find_library(PUGI_LIBRARIES NAMES pugixml)

assert(PUGI_INCLUDE_DIRS AND PUGI_LIBRARIES MESSAGE "Failed to find pugixml")
