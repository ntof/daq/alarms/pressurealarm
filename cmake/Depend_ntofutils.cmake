
include(xProject)
include(Cern)
include(ExtCache)

set(cache_file ${CMAKE_BINARY_DIR}/xProjectCache.cmake)
create_initial_cache(${cache_file})

xProject_Add(NTOFUTILS
    GIT_SUBMODULE
    CMAKE_ARGS -DBUILD_SHARED_LIBS=OFF -DCMAKE_INSTALL_PREFIX=
	-DEXT_CACHE:path=${CMAKE_BINARY_DIR}
        -DEXTERNAL_DEPENDENCIES=ON -C ${cache_file}
    INSTALL_COMMAND $(MAKE) install "DESTDIR=${CMAKE_BINARY_DIR}/instroot"
    LIBRARIES "${CMAKE_BINARY_DIR}/instroot/${CMAKE_INSTALL_LIBDIR}/libntofutils.a"
    INCLUDE_DIRS "${CMAKE_BINARY_DIR}/instroot/${CMAKE_INSTALL_INCLUDEDIR}/ntofutils")
add_dependencies(NTOFUTILS DIM)

add_library(ntofutils STATIC IMPORTED)
set_property(TARGET ntofutils PROPERTY IMPORTED_LOCATION "${NTOFUTILS_LIBRARIES}")
add_dependencies(ntofutils NTOFUTILS)
