cmake_minimum_required(VERSION 2.8)

project(PressureAlarm C CXX)

# Add cmake directory in search path
list(APPEND CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake")

include(Policies)
set_policy(CMP0042 NEW)
set_policy(CMP0043 NEW)
set_policy(CMP0054 NEW)

include(auto_option)

include(Dependencies)
include(Tools)
include(Lint)

include(GitVersion)
set(VERSION_MAJOR "1")
set(VERSION_MINOR "0")
set(VERSION_PATCH "${GIT_COUNT}")
message(STATUS "Building ${CMAKE_PROJECT_NAME}: ${VERSION_MAJOR}.${VERSION_MINOR}.${VERSION_PATCH}")

include(Pack)

use_cxx(17)

set(CMAKE_POSITION_INDEPENDENT_CODE ON)
if(NOT MSVC)
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -pedantic")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -pedantic -Wno-long-long")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wnon-virtual-dtor -Woverloaded-virtual -Wunused-parameter -Wuninitialized")
endif(NOT MSVC)

if(NOT DEFINED BUILD_SHARED_LIBS)
    set(BUILD_SHARED_LIBS ON)
endif()

configure_file("local-config.h.in" "local-config.h")

add_subdirectory(src)
add_subdirectory(tests)
add_subdirectory(data)
